package group.spd.restswaggerdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@SuppressWarnings("checkstyle:HideUtilityClassConstructor")
public class RestSwaggerDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestSwaggerDemoApplication.class, args);
    }
}
