package group.spd.restswaggerdemo.conf;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import springfox.documentation.builders.AlternateTypeBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

import java.lang.reflect.Type;
import java.util.List;

import static springfox.documentation.schema.AlternateTypeRules.newRule;

@Configuration
public class SwaggerConfiguration {

    private final TypeResolver resolver;

    public SwaggerConfiguration(TypeResolver resolver) {
        this.resolver = resolver;
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("group.spd.restswaggerdemo"))
                .build()
                .alternateTypeRules(
                        newRule(resolver.resolve(Sort.class), resolver.resolve(List.class, String.class)),
                        newRule(resolver.resolve(Pageable.class), resolver.resolve(pageableRule()))
                )
                .securitySchemes(apiKeys())
                .securityContexts(List.of(securityContext()))
                .ignoredParameterTypes(AuthenticationPrincipal.class);
    }

    private Type pageableRule() {
        return new AlternateTypeBuilder()
                .fullyQualifiedClassName(String.format("%s.generated.%s", Pageable.class.getPackageName(), Pageable.class.getSimpleName()))
                .property(p -> p.name("page").type(Integer.class).canWrite(true).canRead(true))
                .property(p -> p.name("size").type(Integer.class).canRead(true).canWrite(true))
                .property(p -> p.name("sort").type(Sort.class).canRead(true).canWrite(true))
                .build();
    }

    private List<SecurityScheme> apiKeys() {
        return List.of(new ApiKey("Authorization token", HttpHeaders.AUTHORIZATION, "header"));
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(globalAuth())
                .build();
    }

    private List<SecurityReference> globalAuth() {
        final AuthorizationScope scope = new AuthorizationScope("global", "Access Everything");
        return List.of(new SecurityReference("Authorization token", new AuthorizationScope[]{scope}));
    }
}
