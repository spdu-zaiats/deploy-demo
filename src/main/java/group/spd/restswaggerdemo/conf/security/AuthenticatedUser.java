package group.spd.restswaggerdemo.conf.security;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Set;

public class AuthenticatedUser extends User {

    private final Long id;

    public AuthenticatedUser(Long id, String name, String role) {
        super(name, "", Set.of(new SimpleGrantedAuthority(role)));
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
