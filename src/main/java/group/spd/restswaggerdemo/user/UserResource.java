package group.spd.restswaggerdemo.user;

import group.spd.restswaggerdemo.eror.NotFoundException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequestMapping("/users")
@Api(tags = "User Resource", description = "Describe how to use our resource")
public class UserResource {

    private final UserRepository repository;

    public UserResource(UserRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    @ApiOperation(value = "Updated value there", notes = "Some notes there")
    public Page<User> getAll(UserFilter userFilter, Pageable pageable) {
        return repository.findAll(userFilter.toSpecification(), pageable);
    }

    @GetMapping("/{userId}")
    public User getById(@PathVariable("userId") long id) {
        return repository.findById(id).orElseThrow(() -> new NotFoundException("User with id " + id + " not found."));
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public User create(@RequestBody User user) {
        return repository.save(user);
    }

    @ResponseStatus(NO_CONTENT)
    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        repository.deleteById(id);
    }
}
