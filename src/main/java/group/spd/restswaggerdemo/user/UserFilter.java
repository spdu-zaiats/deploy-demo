package group.spd.restswaggerdemo.user;

import org.springframework.data.jpa.domain.Specification;

public class UserFilter {
    private String firstName;
    private String lastName;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    Specification<User> toSpecification() {
        return byFirstName()
                .and(byLastName());
    }

    private Specification<User> byFirstName() {
        return (root, query, cb) -> firstName == null ? null :
                cb.equal(root.get(User_.firstName), firstName);
    }

    private Specification<User> byLastName() {
        return (root, query, cb) -> lastName == null ? null :
                cb.equal(root.get(User_.lastName), lastName);
    }
}
